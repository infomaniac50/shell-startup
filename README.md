## Shell Startup ##

by Peter Ward https://bitbucket.org/flowblok/shell-startup  
Git Fork by Derek Chafin https://bitbucket.org/infomaniac50/shell-startup  

### A quote from Peter's blog ###

> If you’re a regular shell user, you’ve almost certainly got a .bash_profile or .bashrc script in your home folder, which usually contains various tweaks, such as setting environment variables (adding that directory to $PATH), telling your shell to do clever things (like set -o noclobber) and adding various aliases to commands (like alias please=sudo).
> 
> (If you’re really organised, you’ll have all your dotfiles in a repository somewhere so that you can keep your settings synchronised across all the machines you work on.)
> 
> Anyhow, I suspect that few people know when things like .bash_profile and .bashrc actually get executed. When I started, I just followed people’s advice of putting stuff in .bashrc, and then when it didn’t work, into .bash_profile. I could stop here and describe just the bash startup process (as silly as it is), but there’s a complication in that I switched to zsh a few years ago (and haven’t looked back), but occasionally use bash on machines which don’t have zsh installed.
> 
> In order to handle this nicely then, I need to be able to specify things which are specific to bash or zsh in their own files, and then to specify things which any POSIX-compliant shell (like aliases and environment variables) can understand in a common startup file.

> My solution to this problem is to define some new dotfile folders, one for each shell (.bash/, .zsh/ and .sh/), and one for the shell-independent files (.shell/):

> Read More @ http://blog.flowblok.id.au/2013-02/shell-startup-scripts.html
